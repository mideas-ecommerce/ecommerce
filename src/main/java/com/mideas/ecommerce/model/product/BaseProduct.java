package com.mideas.ecommerce.model.product;

import com.mideas.ecommerce.model.common.BaseModel;

public class BaseProduct extends BaseModel{
	String name;
	String description;
	BaseProductImage image;
}
