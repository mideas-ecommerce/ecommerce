package com.mideas.ecommerce.model;

import java.util.Date;

import com.mideas.ecommerce.model.common.BaseModel;

public class BaseUser extends BaseModel{
	String note;
	String phoneNumber;
	String username;
	String fullName;
	String password;
	String email;
	Date birthday;
	String gender;
	String avatarUrl;
	BaseAddress address;
}
